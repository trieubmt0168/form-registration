# Form-registration

## Using:

- [Tailwindcss](https://tailwindcss.com/)
- [Blueprint.js](https://blueprintjs.com/docs/)
- PostCSS & Autoprefixer

## Up and running:

Install dependencies:

`$ yarn`

Start dev server:

`$ yarn start`

Linting file:

`$ yarn run lint`

Test runner:

`$ yarn test`

Build for prod:

`$ yarn run build`
