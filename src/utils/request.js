import { API_URL } from '../constants/index';

function f(path, options) {
  const e = API_URL;
  return new Promise((resolve, reject) => {
    fetch(`${e}${path}`, options)
      .then((res) => {
        if (res.status !== 200) {
          throw new Error(res.statusText);
        }
        return resolve(res.json());
      })
      .catch((err) => reject(err));
  });
}

export default {
  get(path) {
    const options = {
      method: 'GET',
      credentials: 'include',
    };
    return f(path, options);
  },
  post(path, data) {
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
    };
    if (data) {
      options.body = JSON.stringify(data);
    }
    return f(path, options);
  },
};
