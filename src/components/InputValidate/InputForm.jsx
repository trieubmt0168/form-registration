/* eslint-disable jsx-a11y/label-has-associated-control */
import React from 'react';
import PropTypes from 'prop-types';

function InputForm({
  id, label, value, onSetValue, type, disable, className, classNameForm,
  placeholder, name, validations, message, inValid, maxLengthInput, minLengthInput,
}) {
  const checkValidate = (e) => {
    onSetValue(e);
    validations(e);
  };
  return (
    <div className={classNameForm}>
      <label>
        {' '}
        {label}
        {' '}
      </label>
      <input
        id={id}
        type={type}
        disabled={disable}
        defaultValue={value}
        className={className}
        placeholder={placeholder}
        name={name}
        onInput={checkValidate}
        maxLength={maxLengthInput}
        minLength={minLengthInput}
      />
      {inValid && (
      <div className="w-full block" style={{ paddingLeft: '10px', paddingTop: '5px' }}>
        <span className="text-red-600" style={{ color: 'red' }}>{message}</span>
      </div>
      ) }
    </div>
  );
}

InputForm.defaultProps = {
  id: '',
  value: '',
  label: '',
  name: '',
  message: '',

  type: '',
  placeholder: '',
  disable: false,
  inValid: false,
  maxLengthInput: 99999,
  minLengthInput: 0,
  onSetValue: () => {},
  validations: () => {},
  className: '',
  classNameForm: '',
};
InputForm.propTypes = {
  id: PropTypes.string,
  label: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  onSetValue: PropTypes.func,
  inValid: PropTypes.oneOfType([PropTypes.object, PropTypes.array, PropTypes.bool]),
  type: PropTypes.string,
  placeholder: PropTypes.string,
  name: PropTypes.string,
  message: PropTypes.string,
  validations: PropTypes.func,
  disable: PropTypes.bool,
  maxLengthInput: PropTypes.number,
  minLengthInput: PropTypes.number,
  className: PropTypes.string,
  classNameForm: PropTypes.string,
};
export default InputForm;
