// @ts-nocheck
import { FormGroup, Icon } from '@blueprintjs/core';
import React from 'react';
import PropTypes from 'prop-types';
import { DateInput } from '@blueprintjs/datetime';
import '../style/style.css';

function DateForm({
  id, label, value, onSetValue,
  name,
  validations,
  message,
  inValid,
  className,
  classNameForm,
  placeholder,
}) {
  const checkValidate = (e) => {
    onSetValue(e);
    validations(e);
  };
  return (
    <FormGroup
      label={label}
      labelFor={id}
      className={classNameForm}
    >
      <DateInput
        formatDate={(date) => date.toLocaleDateString()}
        defaultValue={value}
        // value={value}
        parseDate={(str) => Date(str)}
        highlightCurrentDay
        // inputProps={{ id, name }}
        inputProps={{ id, name }}
        className={className}
        placeholder={placeholder}
        onChange={checkValidate}
        timePickerProps
      />
      <Icon icon="calendar" />

      {inValid && (
      <div className="block ffff" style={{ paddingLeft: '10px', paddingTop: '5px' }}>
        <span className="text-red-600">{ message}</span>
      </div>
      ) }
    </FormGroup>
  );
}

DateForm.defaultProps = {
  id: '',
  value: '',
  label: '',
  name: '',
  message: '',
  inValid: false,
  onSetValue: () => {},
  validations: () => {},
  className: '',
  classNameForm: '',
  placeholder: '',
};
DateForm.propTypes = {
  id: PropTypes.string,
  label: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.any]),
  onSetValue: PropTypes.func,
  inValid: PropTypes.oneOfType([PropTypes.object, PropTypes.array, PropTypes.bool]),
  name: PropTypes.string,
  message: PropTypes.string,
  validations: PropTypes.func,
  className: PropTypes.string,
  classNameForm: PropTypes.string,
  placeholder: PropTypes.string,
};
export default DateForm;
