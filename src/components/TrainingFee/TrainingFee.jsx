/* eslint-disable no-unreachable */
/* eslint-disable default-case */
/* eslint-disable consistent-return */
// @ts-nocheck
/* eslint-disable react/button-has-type */
/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { useEffect, useState, Suspense } from 'react';
import '../style/style.css';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
// import { makeStyles } from '@material-ui/core/styles';
import {
  NativeSelect, FormControl, Checkbox, TableCell,
} from '@material-ui/core';
import {
  Button, Intent, Dialog, Spinner,
} from '@blueprintjs/core';
import { useHistory } from 'react-router-dom';
import Action from '../../store/action';
import InputForm from '../InputValidate/InputForm';
import logo from '../../images/logo.png';
import DateForm from '../InputValidate/DateForm';

// const useStyles = makeStyles((theme) => ({
//   container: {
//     display: 'flex',
//     flexWrap: 'wrap',
//   },
//   textField: {
//     marginLeft: theme.spacing(1),
//     marginRight: theme.spacing(1),
//     width: 200,
//   },
// }));

const TrainingFee = ({
  listBranch,
  getBranchs,
  getSuccess,
  onInput,
  onSubmit,
  selectBranch,
  getPending,
  inValidName,
  validateName,
  validateTimeStart,
  inValidNameParent,
  validateParentName,
  inValidTimeStart,
  inValidMobile,
  validateMobile,
  isDisable,
  onDateStart, TimeStart,
}) => {
  const history = useHistory();
  const [isOpen, setOpen] = useState(Boolean);

  const backcate = () => {
    history.go();
  };
  useEffect(() => {
    getBranchs();
  }, []);
  const renderDate = (e) => {
    switch (e) {
      case '1':
        return ' Thứ 2';
      case '2':
        return ' Thứ 3';
      case '3':
        return ' Thứ 4';
      case '4':
        return ' Thứ 5';
      case '5':
        return ' Thứ 6';
      case '6':
        return ' Thứ 7';
      case '7':
        return 'Chủ nhật';
      default:
        return '';
    }
  };

  return (
    <div className="wrapper">
      <div>
        <img src={logo} alt="" className="logo" />
      </div>
      <div className="inner ">
        <div className="form">
          <h3 className="focus:underline">TRẢI NGHIỆM MIỄN PHÍ BÓNG ĐÁ TRẺ EM TẠI YOUNG KIDS</h3>

          <div className="form-wrapper check">
            <label className="text-fiel">
              Vui lòng điền thông tin của học viên vào form bên dưới chúng tôi sẽ liên hệ ngay sau
              khi nhận được thông tin đăng ký. Buổi tập thử với HLV chuyên nghiệp hoàn toàn
              <span className="form-text"> MIỄN PHÍ!!!!</span>
            </label>
          </div>

          <InputForm
            classNameForm="form-wrapper check"
            label="Tên học sinh"
            type="text"
            placeholder="Tên học sinh"
            name="name"
            onSetValue={onInput}
            className="form-control"
            maxLengthInput={50}
            minLengthInput={6}
            validations={validateName}
            inValid={inValidName}
            message="Kí tự không nhỏ hơn 6, không lớn hơn 50"
          />
          <div className="form-wrapper">
            <label>Ngày sinh</label>
            <DateForm
              classNameForm="w-full sm:w-1/2 pickerDate form-wrapper "
              id="date_of_births"
              name="date_of_births"

              className="w-full sm:w-2/3 "
              value={TimeStart}
              onSetValue={onDateStart}
              placeholder="Ngày sinh"
              validations={validateTimeStart}
              inValid={inValidTimeStart}
              message="Chọn ngày sinh phải nhỏ hơn ngày hiện tại."
            />
          </div>
          <InputForm
            classNameForm="form-wrapper check"
            label="Tên phụ huynh"
            type="text"
            placeholder="Tên phụ huynh"
            name="parent_name"
            onSetValue={onInput}
            className="form-control"
            maxLengthInput={50}
            minLengthInput={6}
            validations={validateParentName}
            inValid={inValidNameParent}
            message="Kí tự không nhỏ hơn 6, không lớn hơn 50"
          />

          <InputForm
            classNameForm="form-wrapper check"
            label="Địa chỉ"
            type="text"
            placeholder="Địa chỉ"
            name="address"
            onSetValue={onInput}
            className="form-control"
            maxLengthInput={100}
            minLengthInput={15}
            // validations={validateAddress}
            // inValid={inValidAddress}
            // message="Kí tự không nhỏ hơn 15, không lớn hơn 100"
          />

          <InputForm
            classNameForm="form-wrapper check"
            label="Số điện thoại"
            type="text"
            placeholder="Số điện thoại"
            name="mobile"
            onSetValue={onInput}
            className="form-control"
            maxLengthInput={12}
            minLengthInput={9}
            validations={validateMobile}
            inValid={inValidMobile}
            message="Số điện thoại là số và có 9 đến 12 kí tự"
          />
          {getSuccess && !getPending && (
            <div className="form-group">
              <div className="form-wrapper">
                <label>Tên sân</label>
                <FormControl className="">

                  <NativeSelect onChange={onInput} name="branch_id" className="form_select ">
                    <option placeholder="Hãy chọn sân tập">Hãy chọn sân tập</option>
                    {listBranch.data.length > 0
                      && listBranch.data.map((branch) => (
                        <option value={branch.id} key={branch.id}>
                          {branch.branch_name}
                        </option>
                      ))}
                  </NativeSelect>

                </FormControl>
              </div>
              {listBranch.data.map((br) => (Number(selectBranch) === br.id ? (
                <div className="form-wrapper" key={br.id}>
                  <div className="form-wrapper">
                    <label className={`${br.eras ? 'block' : 'hidden'}`}>Lịch tập</label>
                    {br.eras.split(',').length > 0
                      && br.eras.split(',').map((era) => (era !== '' ? (
                        <TableCell padding="checkbox" key={era}>
                          <label>
                            <Checkbox
                              name="at_eras"
                              inputProps={{ 'aria-label': 'select all desserts' }}
                              onChange={onInput}
                              value={era}
                            />
                            <h5 className="hr text-red">{renderDate(era)}</h5>

                          </label>
                        </TableCell>
                      ) : undefined))}
                  </div>
                </div>
              ) : undefined))}
            </div>
          )}

          <div style={{ paddingTop: '5px' }}>
            <Suspense fallback={<Spinner size={50} intent={Intent.PRIMARY} />}>
              <Button
                disabled={isDisable}
                type="button"
                intent={Intent.SUCCESS}
                onClick={() => {
                  onSubmit();
                  setOpen(!isOpen);
                }}
              >
                Submit
              </Button>
            </Suspense>
          </div>
          <Dialog className="dialog" isOpen={isOpen} canOutsideClickClose usePortal>
            <div className="dialog">
              <h3>
                Xin cảm ơn anh/chị đã quan tâm tới hoạt động huấn luyện bóng đá dành cho trẻ em của
                {' '}
                <label className="text-young">Trung tâm Young Kids</label>
              </h3>

              <Button
                className="my-2 "
                text="Đóng"
                intent={Intent.WARNING}
                type="button"
                onClick={() => backcate()}
              />
            </div>
          </Dialog>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  listBranch: state.getBranch.getBranch,
  getPending: state.getBranch.getBranchPending,
  getSuccess: state.getBranch.getBranchSuccessState,
});
const mapDispatchToProps = (dispatch) => ({
  getBranchs: () => dispatch(Action.GetBranch()),
});
TrainingFee.defaultProps = {
  listBranch: [],
  selectBranch: '',
  inValidName: false,
  inValidNameParent: false,
  // inValidAddress: false,
  inValidMobile: false,
  inValidTimeStart: false,
  isDisable: true,
  onDateStart: () => {},
};
TrainingFee.propTypes = {
  listBranch: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  getSuccess: PropTypes.bool.isRequired,
  getPending: PropTypes.bool.isRequired,
  getBranchs: PropTypes.func.isRequired,
  onInput: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  selectBranch: PropTypes.string,
  inValidName: PropTypes.bool,
  inValidNameParent: PropTypes.bool,
  // inValidAddress: PropTypes.bool,
  inValidMobile: PropTypes.bool,
  //
  validateName: PropTypes.func.isRequired,
  validateParentName: PropTypes.func.isRequired,
  // validateAddress: PropTypes.func.isRequired,
  validateMobile: PropTypes.func.isRequired,
  validateTimeStart: PropTypes.func.isRequired,
  isDisable: PropTypes.bool,
  inValidTimeStart: PropTypes.bool,
  onDateStart: PropTypes.func,
  TimeStart: PropTypes.oneOfType([PropTypes.string, PropTypes.object]).isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(TrainingFee);
