// @ts-nocheck
import { connect } from 'react-redux';
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import TrainingFrees from '../components/TrainingFee/TrainingFee';
import Action from '../store/action';

function TrainingFee({ createForm }) {
  const [data, setData] = useState({
    name: '', mobile: '', parent_name: '', branch_id: '', date_of_birth: new Date(), address: '', at_eras: '',
  });

  const [selectBranch, setBranch] = useState();
  const [atEras, setAt] = useState('');
  const [timeStart, setTimeStart] = useState(new Date());

  const [IsValidateName, setIsValidateName] = useState(false);
  const [IsValidateParentName, setIsValidateParentName] = useState(false);
  const [IsValidateMobile, setValidateMobile] = useState(false);
  const [isDisable, setIsDisable] = useState(true);
  const [inValidTimeStart, setValidTimeStart] = useState(false);

  useEffect(() => {
    if (!IsValidateName && !IsValidateParentName
      && !IsValidateMobile && !inValidTimeStart && data && data.name) {
      setIsDisable(false);
    } else {
      setIsDisable(true);
    }
  }, [IsValidateName, IsValidateParentName, IsValidateMobile, data]);
  const validateTimeStart = (date) => {
    const now = new Date().toLocaleDateString();
    if (date.toLocaleDateString() < now) {
      setValidTimeStart(false);
    } else {
      setValidTimeStart(true);
    }
  };
  const handInput = (e) => {
    const { name, value, checked } = e.target;
    const listEras = [...atEras];
    // const a = listEras.join(',');
    setData({ ...data, [name]: value });
    if (name === 'branch_id') {
      setBranch(value);
    }
    if (name === 'date_of_births') {
      setTimeStart(value);
    }
    if (name === 'at_eras1') {
      if (checked) {
        const a = listEras.join(',');
        setAt(a);
        listEras.push(value);
        setData({ ...data, at_eras: atEras });
      } if (!checked) {
        const removeAt = listEras.filter((fil) => fil !== value);
        setAt(removeAt);
        setData({ ...data, at_eras: atEras });
      }
      console.log(data);
    }
    console.log(data);
  };
  const handleSubmit = () => {
    createForm(data);
    setTimeout(() => {

    }, 3300);
  };
  const validateName = (e) => {
    const { value } = e.target;
    const validateNameregex = new RegExp(/^(.{6,50})$/g);
    if (validateNameregex.test(value)) {
      setIsValidateName(false);
    } else {
      setIsValidateName(true);
    }
  };
  const validateParentName = (e) => {
    const { value } = e.target;
    const validateParentNameregex = new RegExp(/^(.{6,50})$/g);
    if (validateParentNameregex.test(value)) {
      setIsValidateParentName(false);
    } else {
      setIsValidateParentName(true);
    }
  };
  const validateMobile = (e) => {
    const { value } = e.target;
    const validateMobileregex = new RegExp(/[^aA-zZ]{9,12}/g);
    if (validateMobileregex.test(value)) {
      setValidateMobile(false);
    } else {
      setValidateMobile(true);
    }
  };
  const handleDateStart = (date) => {
    setTimeStart(date);
  };
  return (
    <TrainingFrees
      onInput={handInput}
      onSubmit={handleSubmit}
      selectBranch={selectBranch}
      inValidName={IsValidateName}
      validateName={validateName}
      validateMobile={validateMobile}
      validateParentName={validateParentName}
      // validateAddress={validateAddress}
      inValidNameParent={IsValidateParentName}
      validateTimeStart={validateTimeStart}
      inValidTimeStart={inValidTimeStart}
      // inValidAddress={IsValidateAddress}
      inValidMobile={IsValidateMobile}
      TimeStart={timeStart}
      isDisable={isDisable}
      atEras={atEras}
      onDateStart={handleDateStart}
    />

  );
}

const mapDispatchToProps = (dispatch) => ({
  createForm: (
    data,
  ) => {
    dispatch(
      Action.CreateForm(
        data,
      ),
    );
  },
});
TrainingFee.propTypes = {
  createForm: PropTypes.func.isRequired,
};
export default connect(null, mapDispatchToProps)(TrainingFee);
