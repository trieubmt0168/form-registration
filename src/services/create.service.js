import req from '../utils/request';

class CreateService {
  create = (
    data,
  ) => req.post('/public/practices', data);
}
export default new CreateService();
