import req from '../utils/request';

class BranchService {
  getBranch = () => req.get('/public/branches/eras?academyId=1');
}

export default new BranchService();
