import { combineReducers } from 'redux';
import { getBranch, prectices } from '../action/actionType';

const getBranchInitialState = {
  getBranchPending: false,
  getBranchSuccessState: false,
  getBranchFailedState: false,
  getBranchFailedMessage: null,
  getBranch: {},
};
function getBranchReducer(state = getBranchInitialState, action) {
  const { type, payload } = action;
  switch (type) {
    //
    case getBranch.GET_BRANCH:
      return {
        ...state,
        getBranchPending: true,
      };
    case getBranch.GET_BRANCH_SUCCESS:
      return {
        ...state,
        getBranchPending: false,
        getBranchSuccessState: true,
        getBranch: payload,
        getBranchFailedState: false,
        getBranchFailedMessage: null,
      };
    case getBranch.GET_BRANCH_FAILED:
      return {
        ...state,
        getBranchPending: false,
        getBranchSuccessState: false,
        getBranchFailedState: true,
        getBranchFailedMessage: payload,
      };
    default:
      return state;
  }
}

const createFormInitialState = {
  createFormPending: false,
  createFormSuccessState: false,
  createFormFailedState: false,
  createFormFailedMessage: null,
  createForm: {},
};
function createFormReducer(state = createFormInitialState, action) {
  const { type, payload } = action;
  switch (type) {
    //
    case prectices.CREATE_PRECTICES:
      return {
        ...state,
        createFormPending: true,
      };
    case prectices.CREATE_PRECTICES_SUCCESS:
      return {
        ...state,
        createFormPending: false,
        createFormSuccessState: true,
        createForm: payload,
        createFormFailedState: false,
        createFormFailedMessage: null,
      };
    case prectices.CREATE_PRECTICES_FAILED:
      return {
        ...state,
        createFormPending: false,
        createFormSuccessState: false,
        createFormFailedState: true,
        createFormFailedMessage: payload,
      };
    default:
      return state;
  }
}
export default () => combineReducers({
  getBranch: getBranchReducer,
  prectices: createFormReducer,
});
