import { actionCreator, getBranch, prectices } from './actionType';
import BranchService from '../../services/branch.service';
import CreateForms from '../../services/create.service';

function GetBranch() {
  return (dispatch) => {
    dispatch(actionCreator(getBranch.GET_BRANCH));
    return BranchService.getBranch()
      .then((data) => {
        dispatch(actionCreator(getBranch.GET_BRANCH_SUCCESS, data));
      })
      .catch((error) => {
        dispatch(
          actionCreator(getBranch.GET_BRANCH_FAILED, { error: error.message }),
        );
      });
  };
}
function CreateForm(date) {
  return async (dispatch) => {
    dispatch(actionCreator(prectices.CREATE_PRECTICES));
    return CreateForms.create(date).then((data) => {
      dispatch(actionCreator(prectices.CREATE_PRECTICES_SUCCESS, data));
    })
      .catch((error) => {
        dispatch(
          actionCreator(prectices.CREATE_PRECTICES_FAILED, { error: error.message }),

        );
      });
  };
}
export default {
  GetBranch, CreateForm,
};
