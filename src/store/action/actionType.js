function actionCreator(type, payload) {
  return {
    type,
    payload,
  };
}
const getBranch = {
  GET_BRANCH: 'getBranch',
  GET_BRANCH_SUCCESS: 'getBranchSuccess',
  GET_BRANCH_FAILED: 'getBranchFailed',
};

const prectices = {
  CREATE_PRECTICES: 'createPrectices',
  CREATE_PRECTICES_SUCCESS: 'createPrecticesSuccess',
  CREATE_PRECTICES_FAILED: 'createPrecticesFailed',
};

export { actionCreator };
export { getBranch, prectices };
