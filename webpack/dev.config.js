const { merge } = require('webpack-merge');
const MiniCSSExtractPlugin = require('mini-css-extract-plugin');
const common = require('./common.config.js');

module.exports = merge(common, {
  mode: 'development',
  devtool: 'inline-source-map',
  optimization: {
    runtimeChunk: 'single',
    splitChunks: {
      chunks: 'all',
    },
  },
  plugins: [
    new MiniCSSExtractPlugin({
      filename: 'css/[name].bundle.css',
      chunkFilename: 'css/[id].bundle.css',
    }),
  ],
  devServer: {
    host: 'localhost',
    port: '8090',
    inline: true,
    hot: true,
    disableHostCheck: true,
    historyApiFallback: true,
    progress: true,
    publicPath: '/',
  },
});
